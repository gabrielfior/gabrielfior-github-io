# Hi, I'm Gabriel!

I'm currently on a sabbatical, tinkering with Zero-Knowledge Proofs and AI Agents.

Before that, I started, raised VC funds for and failed DeepBalancer, a web3 start-up applying Deep Learning to DeFi investing. As part of my technical CEO duties, I worked mostly on selling our product to clients, liasing with investors, formulating/backtesting potential financial strategies and writing smart contracts.

Previously I studied Software Engineering at Oxford, built software in finance and studied Physics at TUM.


<p float="left">
<a href="https://github.com/gabrielfior">
<img src="images/square-github.svg" height="50" width="50" />
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</a>
<a href="https://www.linkedin.com/in/gabrielfior/">
  <img src="images/linkedin.svg" height="50" width="50" />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
  <a href="https://twitter.com/theGabrielFior">
  <img src="images/square-x-twitter.svg" height="50" width="50" />
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </a>
</p>